﻿using IdentityModel.Client;
using System;
using System.Net.Http;

namespace PwdClient
{
    class Program
    {

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            var diso = await DiscoveryClient.GetAsync("https://localhost:44396");
            if (diso.IsError)
            {
                Console.WriteLine(diso.Error);
            }

            var tokenClient = new TokenClient(diso.TokenEndpoint, "pweClient", "secret");
            var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync("test","123456","api");
            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
            }
            else
            {
                Console.WriteLine(tokenResponse.Json);
            }
            var httpClient = new HttpClient();
            httpClient.SetBearerToken(tokenResponse.AccessToken);
            var response = await httpClient.GetAsync("https://localhost:44318/api/values");
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine(await response.Content.ReadAsStringAsync());
            }
            Console.ReadLine();
            Console.WriteLine("Hello World!");
        }
    }
}
