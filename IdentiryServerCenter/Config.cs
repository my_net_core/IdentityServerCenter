﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentiryServerCenter
{
    public class Config
    {
        public static IEnumerable<ApiResource> GetResource()
        {
            return new List<ApiResource>()
            {  new ApiResource("api", "my api")
        };
        }

        public static IEnumerable<Client> GetClient()
        {
            return new List<Client>()
        {
                new Client()
                {
                     ClientId="client", AllowedGrantTypes=GrantTypes.ClientCredentials, ClientSecrets={ new Secret("secret".Sha256()) },
                     AllowedScopes={ "api"}
                },
                new Client()
                {
                     ClientId="pweClient", AllowedGrantTypes=GrantTypes.ResourceOwnerPassword, ClientSecrets={new Secret("secret".Sha256())}, AllowedScopes={ "api"}
                }
        };
        }
        public static List<TestUser>GetTestUsers()
        {

            return new List<TestUser>()
            {
                new TestUser(){  Password="123456", SubjectId="1", Username="test"}
            };
        }
    }

}
