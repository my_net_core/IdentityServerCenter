﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Mvc;
using MvcAuthSample.Models;
using MvcAuthSample.Servers;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcAuthSample.Controllers
{
    public class ConsentController : Controller
    {
       
        private readonly ConsentService consentService;

        public ConsentController( ConsentService consentService)
        {
            this.consentService = consentService;
        }

        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index(string returnUrl)
        {
            var model = await consentService.BuidConsentViewModel(returnUrl);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(InputConsentViewModel viewModel)
        {
            var result = await consentService.PorcessConsent(viewModel);
            if (result.IsRedirect)
            {
                return Redirect(result.RedirectUrl);
            }
            if (!string.IsNullOrWhiteSpace(result.ValidationError))
            {
                ModelState.AddModelError("", result.ValidationError);
            }
            return View(result.ConsentViewModel);
        }



    }
}
