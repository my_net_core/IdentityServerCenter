﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcAuthSample
{
    public class Config
    {
        //所有可以访问的Resource
        public static IEnumerable<ApiResource> GetApiResource()
        {
            return new List<ApiResource>()
            {  new ApiResource("api", "api application")
        };
        }
        //客户端
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>()
        {
                new Client()
                {
                    ClientId="mvc",
                    ClientName ="Mvc Client",
                    ClientUri ="https://localhost:44385",
                    LogoUri ="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552925177528&di=37cdf95fd0aebce8f087d2538ca2fb59&imgtype=0&src=http%3A%2F%2Fpic2.16pic.com%2F00%2F23%2F49%2F16pic_2349253_b.jpg",
                    AllowRememberConsent =true,
                    AllowedGrantTypes =GrantTypes.Implicit,
                    ClientSecrets ={new Secret("secret".Sha256())},

                    RequireConsent=true,
                    RedirectUris = { "https://localhost:44385/signin-oidc"},//跳转登录到的客户端的地址
                    PostLogoutRedirectUris={ "https://localhost:44385/signout-callback-oidc"},//跳转登出到的客户端的
                    AllowedScopes ={//运行访问的资源
                
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.Address,
                        IdentityServerConstants.StandardScopes.Phone}

                }

        };
        }
        //定义系统中的资源
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>() {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile() };
        }
        public static List<TestUser> GetTestUsers()
        {

            return new List<TestUser>()
            {
                new TestUser(){  Password="123456", SubjectId="1000", Username="test" }
            };
        }
    }
}
