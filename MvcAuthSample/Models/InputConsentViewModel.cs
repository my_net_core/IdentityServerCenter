﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcAuthSample.Models
{
    public class InputConsentViewModel
    {

        public string Button { get; set; }
        public IEnumerable<string> ScopesConented { get; set; }
        /// <summary>
        /// 允许记住同意
        /// </summary>
        public bool RemeberConsent { get; set; }
        public string ReturnUrl { get; set; }
    }
}
