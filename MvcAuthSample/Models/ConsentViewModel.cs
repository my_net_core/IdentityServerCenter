﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcAuthSample.Models
{
    public class ConsentViewModel:InputConsentViewModel
    {

        public string ClientUri { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }

        public string ClientLogoUrl { get; set; }

        public IEnumerable<ScopeViewModel> IdentiryScopes { get; set; }

        public IEnumerable<ScopeViewModel> ResourceScopes { get; set; }

       
    }
}
